module.exports = {
  context: __dirname + '/public',
  entry: __dirname + '/public/src/main.js',
  devtool: 'source-map',
  module: {
    loaders: [
      {
        loader: 'babel',
        test: /\.js$/,
        exclude: /node_modules/,
        query: {
          presets: ['es2015']
        }
      }
    ]
  },

  output: {
    path: __dirname + '/public/dist',
    filename: 'bundle.js'
  }
};
