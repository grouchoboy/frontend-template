var gulp = require('gulp');
var webpack = require('webpack-stream');
var sass = require('gulp-sass');

gulp.task('default', function () {
  gulp.watch('./public/src/*.js', ['webpack']);
  gulp.watch('./public/css/*.scss', ['sass']);
});

gulp.task('dev', () => {
  gulp.watch('./public/src/*.js', ['webpack']);
  gulp.watch('./public/css/*.scss', ['sass']);
});

gulp.task('sass', () => {
  gulp.src('./public/css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('sass:watch', () => {
  gulp.watch('./public/css/*.scss', ['sass']);
});

gulp.task('webpack', () => {
  gulp.src('public/src/main.js')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('public/dist/'));
});

gulp.task('webpack:watch', () => {
  gulp.watch('./public/src/*.js', ['webpack']);
});
